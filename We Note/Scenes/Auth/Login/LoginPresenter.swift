//
//  LoginPresenter.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/4/21.
//

import UIKit

class LoginPresenter: LoginPresenterProtocol, LoginInteractorOutputProtocol {
    
    
    weak var view: LoginViewProtocol?
    private let interactor: LoginInteractorInputProtocol
    private var router: LoginRouterProtocol
    
    
    init(view: LoginViewProtocol, interactor: LoginInteractorInputProtocol, router: LoginRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
     
    func registerOrLoginUser(email: String, password: String) {
        view?.showLoadingIndicator()
        interactor.registerOrLoginUser(email: email, password: password)
    }
    
    
    func fetchedSuccessfuly(userUid: String, message: String) {
        prefs.setValue(userUid, forKey: UD.PrefKeys.userUid)
        view?.hideLoadingIndicator()
        view?.showMessage(message: message)
        view?.successRequest()
    }
    
    func fetchingFailed(withError error: String) {
         view?.hideLoadingIndicator()
         view?.showMessage(message: error)
    }
    
    func goToNoteList() -> UIViewController {
        return router.goToNoteList()
    }
     
}
