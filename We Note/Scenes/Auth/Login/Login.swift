//
//  Login.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/4/21.
//

import UIKit

protocol LoginViewProtocol: class { //View Conteroller
    var presenter: LoginPresenterProtocol! { get set }
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func successRequest()
    func showMessage(message: String)
}

protocol LoginPresenterProtocol: class { // Logic
    var view: LoginViewProtocol? { get set }
    func registerOrLoginUser(email: String, password: String)
    func goToNoteList() -> UIViewController
}

protocol LoginInteractorInputProtocol: class { // func do it from presenter
     var presenter: LoginInteractorOutputProtocol? { get set }
    func registerOrLoginUser(email: String, password: String)
}

protocol LoginInteractorOutputProtocol: class { // it's will call when interactor finished
    func fetchedSuccessfuly(userUid: String, message: String)
    func fetchingFailed(withError error: String) 
}

protocol LoginRouterProtocol { // For Segue
    func goToNoteList() -> UIViewController
}
