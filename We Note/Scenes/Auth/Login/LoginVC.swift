//
//  LoginVC.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/4/21.
//

import UIKit
 
class LoginVC: UIViewController, LoginViewProtocol {
    
    //MARK: - IBOutlets
    @IBOutlet weak private var imageView: UIImageView!
    @IBOutlet weak private var emailTextField: UITextField!
    @IBOutlet weak private var passwordTextField: UITextField!
    @IBOutlet weak private var loginButton: UIButton!
    @IBOutlet weak private var loginButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak private var loadingSpinner: UIActivityIndicatorView!
    
    //MARK: - Properties
    var presenter: LoginPresenterProtocol!
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        animateViews()
    }
    
    func showLoadingIndicator() {
        animateLogin()
    }
    
    func hideLoadingIndicator() {
        stopAnimateLogin()
    }
    
    func successRequest() {
        let viewController = presenter.goToNoteList()
        self.present(viewController, animated: true, completion: nil)
    }
    
    func showMessage(message: String) {
        ToastManager.shared.showMessage(message: message)
    }
    
    //MARK: - IBOutlets
    @IBAction
    func didTappedLogin(_ sender: UIButton) {
        presenter.registerOrLoginUser(email: emailTextField.text ?? "", password: passwordTextField.text ?? "")
    }
}

//MARK: Setup View
extension LoginVC {
    
     fileprivate func setupUI() {
        title = "Login"
        loadingSpinner.isHidden = true
        hideViews()
     }
    
    func hideViews() {
        let scaleDownTransform = CGAffineTransform(scaleX: 0, y: 0)
        imageView.transform = scaleDownTransform
        let scaleLeftTransform = CGAffineTransform(translationX: -UIScreen.main.bounds.width, y: 0)
        emailTextField.transform = scaleLeftTransform
        let scaleRightTransform = CGAffineTransform(translationX: UIScreen.main.bounds.width, y: 0)
        passwordTextField.transform = scaleRightTransform
        
    }
    
    func animateViews() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5) {
                self.imageView.transform = .identity
                self.emailTextField.transform = .identity
                self.passwordTextField.transform = .identity
            }
        }
    }
    
    func animateLogin() {
        self.loginButtonTopConstraint.constant = 100
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        }) { (_) in
            self.loadingSpinner.isHidden = false
            self.loadingSpinner.startAnimating()
        }
    }
    
    
    func stopAnimateLogin() {
        self.loginButtonTopConstraint.constant = 32
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        }) { (_) in
            self.loadingSpinner.isHidden = true
            self.loadingSpinner.stopAnimating()
        }
    }
     
}

 
