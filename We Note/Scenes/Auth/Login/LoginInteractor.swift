//
//  LoginInteractor.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/4/21.
//

import Foundation
import Firebase

class LoginInteractor: LoginInteractorInputProtocol {

    weak var presenter: LoginInteractorOutputProtocol?
    
    func registerOrLoginUser(email: String, password: String) {
        Auth.auth().createUser(withEmail: email , password: password) {[weak self] (result, error) in
            guard let self = self else {return}
            if(error == nil){
                guard let uid = result?.user.uid else { return }
                self.presenter?.fetchedSuccessfuly(userUid: uid, message: ConstantsMessage.accountCreateSucces)
            }else {
                print(" \(error?.localizedDescription ?? "")")
                self.loginUser(email: email, password: password)
            }
        }
    }
 
    func loginUser(email: String, password: String) {
        Auth.auth().signIn(withEmail: email, password: password) { [weak self] (result, error) in
            guard let self = self else {return}
            if(error == nil){
                guard let uid = result?.user.uid else { return }
                self.presenter?.fetchedSuccessfuly(userUid: uid, message: ConstantsMessage.accountLoginSucces)
            } else {
                self.presenter?.fetchingFailed(withError: error?.localizedDescription ?? "")
                print(" \(error?.localizedDescription ?? "")")
            }
        }
    }
     
}
