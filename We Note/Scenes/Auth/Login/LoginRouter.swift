//
//  LoginRouter.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/4/21.
//

import UIKit
 
class LoginRouter: LoginRouterProtocol { //Router is only one take dicret instance
    

    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = UIStoryboard(name: Helper.storyboardName.auth.rawValue, bundle: nil).instantiateViewController(withIdentifier: "\(LoginVC.self)") as! LoginVC
        let interactor =  LoginInteractor()
        let router =  LoginRouter()
        let presenter =  LoginPresenter(view: view, interactor: interactor, router: router)
        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view
        return view
    }
    
    
    func goToNoteList() -> UIViewController {
        let view = UIStoryboard(name: Helper.storyboardName.main.rawValue, bundle: nil).instantiateViewController(withIdentifier: "\(NoteListVC.self)") as! NoteListVC
        let interactor =  NoteListInteractor()
        let router = NoteListRouter()
        let presenter = NoteListPresenter(view: view, interactor: interactor, router: router)
        view.presenter = presenter 
        interactor.presenter = presenter
        router.viewController = view
        let nvg = UINavigationController(rootViewController: view)
        nvg.modalPresentationStyle = .fullScreen
        return nvg
    }

}
