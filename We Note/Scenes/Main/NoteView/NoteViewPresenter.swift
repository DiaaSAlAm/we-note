//
//  NoteViewPresenter.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/5/21.
//

import UIKit

class NoteViewPresenter: NoteViewPresenterProtocol, NoteViewInteractorOutputProtocol {
    
    weak var view: NoteViewViewProtocol?
    private let interactor: NoteViewInteractorInputProtocol
    private var router: NoteViewRouterProtocol
    private var noteId: String
    private var noteModel: NoteListModel?
    let locationManager: LocationManagerProtocol = LocationManager()
    var isAddNewNote: Bool?
    var noteText: String {
        return noteModel?.messageText ?? ""
    }
    
    init(view: NoteViewViewProtocol, interactor: NoteViewInteractorInputProtocol, router: NoteViewRouterProtocol,noteId: String) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.noteId = noteId
        self.isAddNewNote = noteId == "" ? true : false
    }
    
    func viewDidLoad() { 
        switch isAddNewNote {
        case true:
            locationManager.enableLocation()
        default: 
            interactor.observeNotesById(id: noteId)
        } 
    }
    
    func add_updateNote(noteMessage: String) {
        switch isAddNewNote {
        case true:
            interactor.addNewNode(noteMessage: noteMessage) 
        default:
            interactor.updateNotesById(noteId: self.noteId, noteMessage: noteMessage)
        }
    }
    
    
    func fetchedSuccessfuly(model: NoteListModel) {
        noteModel = model
        view?.successRequest()
    }
    
    func noteAdd_updateSuccessfuly(){
        view?.noteAdd_updateSuccessfuly()
    }
     
    
    func fetchingFailed(withError error: String) {
         view?.showMessage(message: error)
    }
    
    func goToNoteInfoVC() -> UIViewController {
        return router.goToNoteInfoVC(noteModel: noteModel)
    }
    
    
     
}
