//
//  NoteView.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/5/21.
//

import UIKit

protocol NoteViewViewProtocol: class { //View Conteroller
    var presenter: NoteViewPresenterProtocol! { get set } 
    func successRequest()
    func noteAdd_updateSuccessfuly()
    func showMessage(message: String)
}

protocol NoteViewPresenterProtocol: class { // Logic
    var view: NoteViewViewProtocol? { get set }
    var isAddNewNote: Bool? {get}
    var noteText: String {get}
    func viewDidLoad()
    func add_updateNote(noteMessage: String)
    func goToNoteInfoVC() -> UIViewController
}

protocol NoteViewInteractorInputProtocol: class { // func do it from presenter
     var presenter: NoteViewInteractorOutputProtocol? { get set }
    func observeNotesById(id: String)
    func updateNotesById(noteId: String,noteMessage: String)
    func addNewNode(noteMessage: String)
    
}

protocol NoteViewInteractorOutputProtocol: class { // it's will call when interactor finished
    func fetchedSuccessfuly(model: NoteListModel)
    func noteAdd_updateSuccessfuly()
    func fetchingFailed(withError error: String)
}

protocol NoteViewRouterProtocol { // For Segue
    func goToNoteInfoVC(noteModel: NoteListModel?) -> UIViewController
}
