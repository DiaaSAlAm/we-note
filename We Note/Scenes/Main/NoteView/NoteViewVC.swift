//
//  NoteViewVC.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/5/21.
//

import UIKit

class NoteViewVC: UIViewController, NoteViewViewProtocol, UITextViewDelegate {
    
    //MARK: - IBOutlets
    @IBOutlet weak private var noteTextView: UITextView!
    @IBOutlet weak private var infoBarButtonItem: UIBarButtonItem!
    @IBOutlet weak private var saveButtonBottomConstraint: NSLayoutConstraint!
    //MARK: - Properties
    var presenter: NoteViewPresenterProtocol!
    private let headerAttributes = [NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: UIFont.TextStyle.headline)]
    private let bodyAttributes = [NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body)]
     
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if presenter.isAddNewNote == true {
            noteTextView.becomeFirstResponder()
        }
    }
    
    func noteAdd_updateSuccessfuly() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func successRequest() {
        let text = noteTextView.text
        noteTextView.text = text == "" ? presenter.noteText : text
        highlightFirstLineInTextView(textView: noteTextView)
    }
    
    func showMessage(message: String) {
        ToastManager.shared.showMessage(message: message)
    }
    
    deinit {
        removeObserverKeyboardNotification()
    }
    
    //MARK: - IBOutlets
    @IBAction
    func didTappedDone(_ sender: UIButton) {
        presenter.add_updateNote(noteMessage: self.noteTextView.text)
        sender.isEnabled = false
    }
    
    @IBAction
    func didTappedInfo(_ sender: Any) {
        let viewController = presenter.goToNoteInfoVC()
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

//MARK: Setup View
extension NoteViewVC {
    
    private func setupUI() {
        addObserverKeyboardNotification()
        presenter.viewDidLoad()
        noteTextView.delegate = self
        if presenter.isAddNewNote == true {
            self.navigationItem.rightBarButtonItem = nil 
        } 
        
     }
    
    private func highlightFirstLineInTextView(textView: UITextView) {
        let textAsNSString = textView.text as NSString
        let lineBreakRange = textAsNSString.range(of: "\n")
        let newAttributedText = NSMutableAttributedString(attributedString: textView.attributedText)
        let boldRange: NSRange
        if lineBreakRange.location < textAsNSString.length {
            boldRange = NSRange(location: 0, length: lineBreakRange.location)
        } else {
            boldRange = NSRange(location: 0, length: textAsNSString.length)
        }
        newAttributedText.addAttribute(NSAttributedString.Key.font, value: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.headline), range: boldRange)
        textView.attributedText = newAttributedText
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let textAsNSString = noteTextView.text as NSString
        let replaced = textAsNSString.replacingCharacters(in: range, with: text) as NSString
        let boldRange = replaced.range(of: "\n")
        
        if boldRange.location <= range.location {
            self.noteTextView.typingAttributes = bodyAttributes
        } else {
            self.noteTextView.typingAttributes = headerAttributes
        }
        return true
    }  
    
    private func hideSaveButton() {
        self.saveButtonBottomConstraint.constant = -100
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
     
    private func showSaveButton(height: CGFloat) {
        self.saveButtonBottomConstraint.constant = height
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
     
    private func addObserverKeyboardNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleShow),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleHide),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func handleShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let height  = keyboardRectangle.height
            showSaveButton(height: height)
        }
    }
    
    @objc func handleHide(){
        showSaveButton(height: 16)
    }
    
    private func removeObserverKeyboardNotification() {
        NotificationCenter.default.removeObserver(self)
    }
 
 
}

 
