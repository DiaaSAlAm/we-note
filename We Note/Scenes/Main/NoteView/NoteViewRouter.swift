//
//  NoteViewRouter.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/5/21.
//

import UIKit
 
class NoteViewRouter: NoteViewRouterProtocol { //Router is only one take dicret instance
    

    weak var viewController: UIViewController?
    
    func goToNoteInfoVC(noteModel: NoteListModel?) -> UIViewController {
        let view = UIStoryboard(name: Helper.storyboardName.main.rawValue, bundle: nil).instantiateViewController(withIdentifier: "\(NoteInfoVC.self)") as! NoteInfoVC
        let interactor = NoteInfoInteractor()
        let router = NoteInfoRouter()
        let presenter = NoteInfoPresenter(view: view, interactor: interactor, router: router, noteModel: noteModel)
        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view
        return view
    }

}
