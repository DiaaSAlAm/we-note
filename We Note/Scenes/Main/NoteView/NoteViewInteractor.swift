//
//  NoteViewInteractor.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/5/21.
//

import Foundation
import Firebase

class NoteViewInteractor: NoteViewInteractorInputProtocol {

    weak var presenter: NoteViewInteractorOutputProtocol?
    let ref = Database.database().reference()
    
    //MARK: observe Notes by Current User UID
    func observeNotesById(id: String){
        let uid = Helper.instance.userUid
        let allNotes = ref.child("UsersNote").child(uid).child("AllNote").child(id)
        allNotes.observe(.value) { [weak self] snapshot in
            guard let self = self else {return}
            self.conversationsData(from: snapshot)
        }
    }
    
 
    func conversationsData(from snapshot: DataSnapshot){
        if let dictionary = snapshot.value as? [String : Any] {
            let note = NoteListModel(dictionary: dictionary)
            self.presenter?.fetchedSuccessfuly(model: note)
        }
    }
    
    
    func addNewNode(noteMessage: String){
      let uid = Helper.instance.userUid
      let ref = Database.database().reference()
        let note = ref.child("UsersNote").child(uid).child("AllNote").childByAutoId()
        let dataArray : [String : Any] = ["noteId" : note.key ?? "", "messageText" : noteMessage ,"createDate" : ServerValue.timestamp(),"lastModifiedDate": ServerValue.timestamp(), "latitude": Helper.instance.latitude , "longitude": Helper.instance.longitude]
        note.setValue(dataArray) {[weak self] (err, reference) in
              guard let self = self else { return }
                if err == nil {
                    print("Success reference \(reference)")
                    self.presenter?.noteAdd_updateSuccessfuly()
                }
            }
        }
    
    
    func updateNotesById(noteId: String,noteMessage: String){
        let uid = Helper.instance.userUid
        let updateNote = ref.child("UsersNote").child(uid).child("AllNote").child(noteId)
        let dataDictionry : [String : Any] = ["messageText" : noteMessage,"lastModifiedDate": ServerValue.timestamp()]
        updateNote.updateChildValues(dataDictionry) { (err, reference) in
            if err == nil {
                print("User Set successfuly")
                self.presenter?.noteAdd_updateSuccessfuly()
            }
        }
    }
     
}
