//
//  NoteListModel.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/4/21.
//
 
 
import Foundation

struct NoteListModel {
    var noteId: String?
    var messageText: String?
    var createDate: Int?
    var lastModifiedDate: Int?
    var latitude: Double?
    var longitude: Double?
    
    
    init(dictionary: [String : Any]) {
        noteId = dictionary["noteId"] as? String 
        messageText = dictionary["messageText"] as? String
        createDate = dictionary["createDate"] as? Int
        lastModifiedDate = dictionary["lastModifiedDate"] as? Int
        latitude = dictionary["latitude"] as? Double
        longitude = dictionary["longitude"] as? Double
        
    }
} 
