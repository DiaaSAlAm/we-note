//
//  NoteListPresenter.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/4/21.
//

import UIKit

class NoteListPresenter: NoteListPresenterProtocol, NoteListInteractorOutputProtocol {
    
    weak var view: NoteListViewProtocol?
    private let interactor: NoteListInteractorInputProtocol
    private var router: NoteListRouterProtocol
    var noteListModel: [NoteListModel]?
    
    var numberOfRow: Int {
        return noteListModel?.count ?? 0
    }
    init(view: NoteListViewProtocol, interactor: NoteListInteractorInputProtocol, router: NoteListRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        interactor.observeNotes()
    }
    
    func fetchedSuccessfuly() {
        view?.successRequest()
    }
    
    func fetchedSuccessfuly<T>(noteListModel: [T]) {
        if ((noteListModel as? [NoteListModel]) != nil) {
          self.noteListModel = noteListModel as? [NoteListModel] ?? []
            self.view?.successRequest()
        }
    }
    
    func fetchingFailed(withError error: String) {
        view?.showMessage(message: error)
    }
    
    func showMessage(message: String) {
        view?.showMessage(message: message)
    }
    
    func noteId(_ row: Int) -> String{
        return noteListModel?.getElement(at: row)?.noteId ?? ""
    }
     
    func messageText(_ row: Int) -> String{
        let messageText = noteListModel?.getElement(at: row)?.messageText ?? ""
        let message = messageText == "" ? "Empty title" : messageText
        return message
    }
    
    func removeNoteById(_ row: Int) { 
        interactor.removeNoteById(noteId: noteId(row))
        noteListModel?.remove(at: row)
    }
    
    func goToNoteViewVC(noteId: String) -> UIViewController {
        return router.goToNoteViewVC(noteId: noteId)
    }
    
}
