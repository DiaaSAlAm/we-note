//
//  NoteListInteractor.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/4/21.
//

import Firebase

class NoteListInteractor: NoteListInteractorInputProtocol {

    weak var presenter: NoteListInteractorOutputProtocol?
    let ref = Database.database().reference()
    var noteListModel = [NoteListModel]()
    
    //MARK: observe Notes by Current User UID
    func observeNotes(){
        let uid = Helper.instance.userUid
        noteListModel.removeAll()
        let allNotes = ref.child("UsersNote").child(uid).child("AllNote")
        allNotes.observe(.value) { [weak self] snapshot in
            guard let self = self else {return}
            self.conversationsData(from: snapshot)
            self.noteListModel.sort(by: {$0.lastModifiedDate ?? 0 > $1.lastModifiedDate ?? 0 })
            self.presenter?.fetchedSuccessfuly(noteListModel: self.noteListModel)
        }
    }
    
 
    func conversationsData(from snapshot: DataSnapshot){
        if let dataArray = snapshot.value as? [String : Any] {
            for snap in dataArray.values { 
                if let dictionary = snap as? [String : Any] {
                    let note = NoteListModel(dictionary: dictionary)
                    let index = self.noteListModel.firstIndex(where: {$0.noteId == note.noteId})  
                    if index != nil {
                        self.noteListModel.remove(at: index!)
                    }
                    self.noteListModel.insert(note, at: index ?? 0)
                }
            }
        }
    }
    
    
    
    func removeNoteById(noteId : String){
        let uid = Helper.instance.userUid
        ref.child("UsersNote").child(uid).child("AllNote").child(noteId).removeValue {[weak self] (err, referance) in
            guard let self = self else {return}
            if err != nil {
                self.presenter?.fetchingFailed(withError: err?.localizedDescription ?? "")
            }else{
                self.observeNotes()
            }
        }
    } 
    
}

 
