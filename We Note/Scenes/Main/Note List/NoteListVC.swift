//
//  NoteListVC.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/4/21.
//

import UIKit
import Firebase

class NoteListVC: UIViewController, NoteListViewProtocol {
    
    //MARK: - IBOutlets
    @IBOutlet private weak var tableView: UITableView!
   
    //MARK: - Properties
    var presenter: NoteListPresenterProtocol!
    private let cell = Helper.cells.noteList.rawValue
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    } 
    
    func successRequest() {
        tableView.reloadData()
    }
    
    func showMessage(message: String) {
        ToastManager.shared.showMessage(message: message)
    }
    
    //MARK: - IBOutlets
    @IBAction
    func didTappedAddNewNote(_ sender: UIButton) {
           gotoNote()
    }
}

//MARK: Setup View
extension NoteListVC {
    
     fileprivate func setupUI() {
        title = "All Notes"
        registerTableView()
        presenter.viewDidLoad()
     }
    
    fileprivate func registerTableView() {
        tableView.register(UINib(nibName: cell, bundle: nil), forCellReuseIdentifier: cell)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
    }
    
     
    func gotoNote(_ noteId: String? = nil){
        let viewController = presenter.goToNoteViewVC(noteId: noteId ?? "")
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

 
// MARK: - UITableView Delegate
extension NoteListVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        defer { tableView.deselectRow(at: indexPath, animated: true) }
        let noteId = presenter.noteId(indexPath.row)
        gotoNote(noteId)
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            presenter.removeNoteById(indexPath.row)
        }
    }
    
    
}

// MARK: - UITableView Data Source
extension NoteListVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRow
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cell) as! NoteListCell  
        cell.attributedText(title: presenter.messageText(indexPath.row))
        return cell
    }
}
