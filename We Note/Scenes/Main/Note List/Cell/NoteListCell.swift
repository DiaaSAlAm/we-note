//
//  NoteListCell.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/4/21.
//

import UIKit

class NoteListCell: UITableViewCell {

    @IBOutlet weak var noteTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code 
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func attributedText(title:String) {
        let nsString = NSString(string: title)
        let attributedString = NSMutableAttributedString(string: nsString as String, attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        let boldFontAttribute = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 18), NSAttributedString.Key.foregroundColor: UIColor.black]
        let range = title.lineRange(for: ..<title.startIndex)
        let nsRange = NSRange(range, in: title)
        attributedString.addAttributes(boldFontAttribute, range: nsRange)
        noteTitleLabel.attributedText = attributedString
    }
    
}
