//
//  NoteList.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/4/21.
//

import UIKit

protocol NoteListViewProtocol: class { //View Conteroller
    var presenter: NoteListPresenterProtocol! { get set } 
    func successRequest()
    func showMessage(message: String)
}

protocol NoteListPresenterProtocol: class { // Logic
    var view: NoteListViewProtocol? { get set }
    func viewDidLoad()
    var numberOfRow: Int {get}
    func messageText(_ row: Int) -> String
    func noteId(_ row: Int) -> String
    func removeNoteById(_ row : Int)
    var noteListModel: [NoteListModel]? {get set}
    func goToNoteViewVC(noteId: String) -> UIViewController
}

protocol NoteListInteractorInputProtocol: class { // func do it from presenter
     var presenter: NoteListInteractorOutputProtocol? { get set }
    func observeNotes()
    func removeNoteById(noteId : String)
}

protocol NoteListInteractorOutputProtocol: class { // it's will call when interactor finished
    func fetchedSuccessfuly<T>(noteListModel: [T])
    func fetchingFailed(withError error: String)
    func showMessage(message: String)
}

protocol NoteListRouterProtocol { // For Segue
    func goToNoteViewVC(noteId: String) -> UIViewController
}
