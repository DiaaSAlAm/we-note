//
//  NoteListRouter.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/4/21.
//

import UIKit
 
class NoteListRouter: NoteListRouterProtocol { //Router is only one take dicret instance
    

    weak var viewController: UIViewController?
    
    func goToNoteViewVC(noteId: String) -> UIViewController {
        let view = UIStoryboard(name: Helper.storyboardName.main.rawValue, bundle: nil).instantiateViewController(withIdentifier: "\( NoteViewVC.self)") as! NoteViewVC
        let interactor = NoteViewInteractor()
        let router = NoteViewRouter()
        let presenter = NoteViewPresenter(view: view, interactor: interactor, router: router, noteId: noteId)
        view.presenter = presenter 
        interactor.presenter = presenter
        router.viewController = view
        return view
    }

}
