//
//  NoteInfo.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/5/21.
//

import UIKit

protocol NoteInfoViewProtocol: class { //View Conteroller
    var presenter: NoteInfoPresenterProtocol! { get set }
}

protocol NoteInfoPresenterProtocol: class { // Logic
    var view: NoteInfoViewProtocol? { get set }
    func noteTitle() -> String
    var dateModified: String { get }
    var dateCreated: String { get } 
    var longitude: Double { get }
    var latitude: Double { get }
}

protocol NoteInfoInteractorInputProtocol: class { // func do it from presenter
     var presenter: NoteInfoInteractorOutputProtocol? { get set }
    
}

protocol NoteInfoInteractorOutputProtocol: class { // it's will call when interactor finished
     
}

protocol NoteInfoRouterProtocol { // For Segue
     
}
