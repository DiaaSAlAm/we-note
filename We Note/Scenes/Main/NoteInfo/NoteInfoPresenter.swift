//
//  NoteInfoPresenter.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/5/21.
//

import UIKit

class NoteInfoPresenter: NoteInfoPresenterProtocol, NoteInfoInteractorOutputProtocol {
    
    weak var view: NoteInfoViewProtocol?
    private let interactor: NoteInfoInteractorInputProtocol
    private var router: NoteInfoRouterProtocol
    private var noteModel: NoteListModel?
    
    var latitude: Double {
        return noteModel?.latitude ?? 0
    }
    var longitude: Double {
        return noteModel?.longitude ?? 0
    }
    
    init(view: NoteInfoViewProtocol, interactor: NoteInfoInteractorInputProtocol, router: NoteInfoRouterProtocol,noteModel: NoteListModel?) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.noteModel = noteModel
    }
     
    
    func noteTitle() -> String {
        let str =  noteModel?.messageText ?? ""
        let range = str.lineRange(for: ..<str.startIndex)
        let mySubstring = str[range]
        return String(mySubstring)
    }
    
    func convertTimestampToString(timestamp: Int) -> String{
        let date = NSDate(timeIntervalSince1970: TimeInterval((timestamp)/1000))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm a"
        return dateFormatter.string(from: date as Date)
    }
    
    var dateCreated: String {
        return convertTimestampToString(timestamp: noteModel?.createDate ?? 0)
    }
    
    var dateModified: String {
        return convertTimestampToString(timestamp: noteModel?.lastModifiedDate ?? 0)
    }
     
     
}
