//
//  NoteInfoVC.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/5/21.
//

import UIKit
import MapKit

class NoteInfoVC: UIViewController, NoteInfoViewProtocol {
    
    //MARK: - IBOutlets
    @IBOutlet private weak var noteTitleLabel: UILabel!
    @IBOutlet private weak var dateCreatedLabel: UILabel!
    @IBOutlet private weak var dateModifiedLabel: UILabel!
    @IBOutlet private weak var mapView: MKMapView!
    
    //MARK: - Properties
    var presenter: NoteInfoPresenterProtocol! 
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
     
    fileprivate func setupUI() {
       setData()
    }
   
   func addPin() {
       let annotation = MKPointAnnotation()
       let centerCoordinate = CLLocationCoordinate2D(latitude: presenter.latitude, longitude:presenter.longitude)
       annotation.coordinate = centerCoordinate
       mapView.centerCoordinate = centerCoordinate
       mapView.addAnnotation(annotation)
   }
    
    
   
   func setData(){
       noteTitleLabel.text = presenter.noteTitle()
       dateCreatedLabel.text = presenter.dateCreated
       dateModifiedLabel.text = presenter.dateModified
       addPin()
   }
    
}
 

 
