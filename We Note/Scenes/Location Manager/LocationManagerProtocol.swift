//
//  LocationManagerProtocol.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/4/21.
//

import Foundation

protocol LocationManagerProtocol: class {
    func enableLocation() 
}
