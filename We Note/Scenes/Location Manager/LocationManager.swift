//
//  LocationManager.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/4/21.
//

import UIKit
import CoreLocation

class LocationManager: NSObject, CLLocationManagerDelegate, LocationManagerProtocol {
    
    private let window = (SceneDelegate.shared?.window)
    private var locationManager: CLLocationManager? = CLLocationManager()
    
     override init() {
        super.init()
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        locationManager?.distanceFilter = 100
        locationManager?.requestWhenInUseAuthorization()
        locationManager?.requestAlwaysAuthorization()
    }
    
     func enableLocation(){
        locationManager?.delegate = self
        locationManager?.startUpdatingLocation()
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("in location")
        
        if let location = locations.first { 
            prefs.setValue(location.coordinate.latitude, forKey: UD.PrefKeys.latitude)
            prefs.setValue(location.coordinate.longitude, forKey: UD.PrefKeys.longitude)
            locationManager?.stopUpdatingLocation()
        } 
    }
     
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("failed location \(error)")
        if let clErr = error as? CLError {
            switch clErr {
            case CLError.locationUnknown:
                print("location unknown")
            case CLError.denied:
                print("denied")
                 showAlert()
            default:
                print("other Core Location error")
            }
        } else {
            print("other error:", error.localizedDescription)
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("in location change auth", status.rawValue)
        if status == .denied  || status == .restricted  {
            showAlert()
        }
    }
    
    func showAlert(){
        let alertController = UIAlertController(title: "Enable GPS", message: "We need permission to determine your location.", preferredStyle: .alert)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel)
        let openSettingAction: UIAlertAction = UIAlertAction(title: "Settings", style: .default) { action in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(cancelAction)
        alertController.addAction(openSettingAction)
        window?.rootViewController?.present(alertController, animated: true, completion: nil)
    } 
}

 
