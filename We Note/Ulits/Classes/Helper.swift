//
//  Helper.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/4/21.
//

import Foundation

public class Helper {
    
    /// Singletone instance
    public static let instance: Helper = Helper()
    
    // singletone
    private init(){}
    
    
    enum controllerID: String{
        case loginVC = "LoginVC"
    }
    
    enum storyboardName: String{
        case auth = "Auth"
        case main = "Main"
        
    }
    
    enum cells: String {
        case noteList = "NoteListCell"
    }
   
    
    var isLogin : Bool {
        get { return prefs.bool(forKey: UD.PrefKeys.isLogin) }
        set { prefs.set(newValue, forKey: UD.PrefKeys.isLogin) }
    }
    
    var userUid : String {
        get { return prefs.string(forKey: UD.PrefKeys.userUid) ?? "" }
        set { prefs.set(newValue, forKey: UD.PrefKeys.userUid) }
    }
    
    var latitude : Double {
        get { return prefs.double(forKey: UD.PrefKeys.latitude) }
        set { prefs.set(newValue, forKey: UD.PrefKeys.latitude) }
    }
    
    var longitude : Double {
        get { return prefs.double(forKey: UD.PrefKeys.longitude) }
        set { prefs.set(newValue, forKey: UD.PrefKeys.longitude) }
    }
    
}


