//
//  ExUserDefaults.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/4/21.
//

import UIKit

typealias UD = UserDefaults

var prefs: UserDefaults {
    return UserDefaults.standard
}

extension UserDefaults {
    
    enum PrefKeys {
        static let isLogin = "isLogin"
        static let userUid = "userUid"
        static let latitude = "latitude"
        static let longitude = "longitudeo"
       
        
    }
    
    func set<T>(encodable object: T?, forKey key: String) where T: Encodable {
        guard let object = object else {
            prefs.set(nil, forKey: key)
            return
        }
        if let encoded = try? JSONEncoder().encode(object) {
            prefs.set(encoded, forKey: key)
        }
    }
    
    func decodable<T>(forKey key: String, of type: T.Type) -> T? where T: Decodable {
        guard let jsonData = prefs.data(forKey: key),
            let object = try? JSONDecoder().decode(type, from: jsonData) else {
                return nil
        }
        return object
    }
}
