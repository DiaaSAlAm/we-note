//
//  ExArray.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/4/21.
//

import Foundation

extension Array {
    func getElement(at index: Int) -> Element? {
        let isValidIndex = index >= 0 && index < count
        return isValidIndex ? self[index] : nil
    }
}
