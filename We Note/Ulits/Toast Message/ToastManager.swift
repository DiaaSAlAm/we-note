//
//  ToastManager.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/4/21.
//

import Foundation
import UIKit

class ToastManager {
    //MARK: Properties
    static let shared = ToastManager()
    private let window = (SceneDelegate.shared?.window)
    private var message: String = ""
    private var bottomAnchor: NSLayoutConstraint!
    private var messageHeaders: [ToastView?] = []
    
    //MARK: Methods
    private init() {}
    
    
    func showMessage(message: String, completion: (() -> Void)? = nil) {
        let messageHeader: ToastView? = ToastView()
        messageHeaders.forEach({
            hideBanner(messageHeaders: $0)
        })
        messageHeaders.append(messageHeader)
        self.message = message
        createBannerWithInitialPosition(errorHeader: messageHeader)
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) { [weak self] in
            self?.hideBanner(messageHeaders: messageHeader)
            completion?()
        }
    }
    
    
    private func createBannerWithInitialPosition(errorHeader: ToastView?) {
        guard let errorHeader = errorHeader else { return }
        guard let window = window else {return}
        errorHeader.errorLabel.text = message
        errorHeader.layer.cornerRadius = 10
        errorHeader.layer.masksToBounds = true
        window.addSubview(errorHeader)
        let guide = window.safeAreaLayoutGuide
        errorHeader.translatesAutoresizingMaskIntoConstraints = false
        bottomAnchor = errorHeader.bottomAnchor.constraint(equalTo: guide.bottomAnchor, constant: 100)
        bottomAnchor.isActive = true
        errorHeader.trailingAnchor.constraint(lessThanOrEqualTo: guide.trailingAnchor, constant: -20).isActive = true
        errorHeader.leadingAnchor.constraint(greaterThanOrEqualTo: guide.leadingAnchor, constant: 20).isActive = true
        errorHeader.centerXAnchor.constraint(equalTo: window.centerXAnchor).isActive = true
        errorHeader.heightAnchor.constraint(equalToConstant: errorHeader.viewHeight).isActive = true
        window.layoutIfNeeded()
        animateBannerPresentation()
    }
    
    private func animateBannerPresentation() {
        KeyboardStateManager.shared.start()
        if KeyboardStateManager.shared.isVisible {
            bottomAnchor.constant = -KeyboardStateManager.shared.keyboardOffset
        } else {
            bottomAnchor.constant = -20
        }
        UIView.animate(withDuration: 0.3, delay: 0.1, options: [], animations: { [weak self] in self?.window!.layoutIfNeeded() }, completion: nil)
    }
    
    private func hideBanner(messageHeaders: ToastView?) {
        KeyboardStateManager.shared.stop()
        UIView.animate(withDuration: 0.5, animations: {
            messageHeaders?.alpha = 0
        }) { _ in
            messageHeaders?.removeFromSuperview()
        }
    }
}
