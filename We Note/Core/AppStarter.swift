//
//  AppStarter.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/4/21.
//

import Foundation
import IQKeyboardManagerSwift

/// AppStarter here you can handle everything before letting your app starts
final class AppStarter {
    static let shared = AppStarter()
    private let window = (SceneDelegate.shared?.window)
    private init() {}
    
    func start() {
        AppTheme.apply()
        FirebaseConfigure.configure()
        setupKeyboardConfig()
        setRootViewController() 
    }
    
    
    private func setupKeyboardConfig() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true 
    }
    
    private func setRootViewController() {
        let nvg = UINavigationController(rootViewController: LoginRouter.createModule())
        window?.rootViewController = nvg
        window?.makeKeyAndVisible()
    }
}
