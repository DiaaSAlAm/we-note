//
//  AppTheme.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/4/21.
//

import UIKit

/// Apply application theme and colors
class AppTheme {
    
    static func apply() {
        navigationBarStyle() 
    }
    
    private static func navigationBarStyle() {
        UINavigationBar.appearance().barTintColor = ConstantsColors.mainColor
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]
        UINavigationBar.appearance().isTranslucent = false
    }
}
