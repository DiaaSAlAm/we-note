//
//  FirebaseConfigure.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/4/21.
//

import Firebase

/// Apply Firebase Database and Notification
class FirebaseConfigure {
    
    static func configure() {
        FirebaseApp.configure()
    }
    
     
}
