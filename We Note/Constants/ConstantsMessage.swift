//
//  ConstantsMessage.swift
//  We Note
//
//  Created by Diaa SAlAm on 2/4/21.
//

import Foundation

struct ConstantsMessage {
    static let genericError = "Something went wrong please try again later"
    static let accountCreateSucces = "Accout Create Successfuly"
    static let accountLoginSucces = "Login Successfuly"
}
